    title: Eerste Design Challenege opdracht
    subtitle: 
    date: 2017-04-09


# 4 september | Maandag #

> *Het was echt wenen dat ik na de weekend en de vakantie weer terug naar school ging.*

Als groep kregen we het opdracht ‘Design Challenge’ uitgebreid te horen. Samen met mijn groep brainstormen we over de game.  We bespreken wie onze doelgroep zal zijn, wat voor soort game we willen en deden ook de taakverdeling. Als taak kreeg ik: onderzoek doen naar ‘aantrekkelijk ontwerp’.

----------


**Mijn ideeen voor het spel was:**

- Sims-achtig & dierenachtig (eigen character)
- Punten scoren door opdrachten uit te voeren zoals, naar museum te gaan.
- Met de punten kun de speler spullen kopen of upgraden.
- online -> gsp/locatie aan & offline -> eigen kamertje met spullen die je had gekocht met punten.

----------

Wij hadden geen workshop deze week dus gingen we weer aan de slag met Design Challenge.
Toen was de dag afgelopen.
