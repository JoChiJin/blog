<h1> Week 1: 31 augustus- 7 september </h1>

<h3> Donderdag 31 augustus - officiële kick-off </h3>

<p> Vandaag was de officiële kick-off van onze eerste Design Challenge. Ik was best wel nieuwsgierig wat de Design Challenge inhoudt. We kregen allerlei informaties mee over de school, opleiding en Design Challenge. Ik vond het meteen interessant. Het leek me erg leuk om een spel te verzinnen. </p>

<p> Vervolgens kregen we tijdens het hoorcollege uitleg over games. Zo legde de meneer uit wat games zijn, wat de doelen zijn van de makers van de games en  Ik vond het best wel interessant. Ik hiel de aantekeningen erbij. 
Na de hoorcollege ging ik met mijn groep een Rotterdamse tour doen. Wij begonnen op school en we kregen een papier met de informatie oftewel hint over de volgende plek waar we heen moeten gaan. Het was leuk, maar ook best wel vermoeiend. Het was erg warm die dag en we liepen meer dan twee uurtjes. Rond drie uur vonden we dat het genoeg was en gingen we terug naar school en liet onze kaart zien aan een mevrouw. </p>
 
---
<h3> Maandag 4 september – Design Challenge </h3>

> Het was echt wenen dat ik na het weekend en de vakantie weer terug naar school ging.

<p>Als groep kregen we het opdracht ‘Design Challenge’ uitgebreid te horen. Samen met mijn groep brainstormen we over de game.  We bespreken wie onze doelgroep zal zijn, wat voor soort game we willen en deden ook de taakverdeling. Als taak kreeg ik: onderzoek doen naar ‘aantrekkelijk ontwerp’. </p>

<b> Mijn ideeen voor het spel was: </b>

- Sims-achtig & dierenachtig (eigen character)
- Punten scoren door opdrachten uit te voeren zoals, naar museum te gaan.
- Met de punten kun de speler spullen kopen of upgraden.
- Online -> gsp/locatie aan & offline -> eigen kamertje met spullen die je had gekocht met punten.

Wij hadden geen workshop deze week dus gingen we weer aan de slag met Design Challenge.
Toen was de dag afgelopen.
 
----

<h3> Woensdag 6 september </h3>

<p> Om tien voor negen stapte ik naar buiten en nam de tram richting de school. Buiten was het mooi weer, maar ook beetje koud. Eenmaal als ik daar ben, ging ik naar het klaslokaal. Om half begonnen we met de uitleg over het rooster, keuzevakken, Design Challenge enz.

Ten eerste ging ik een onderzoek doen naar de winkels in Rotterdam met het onderzoek vraag: "Welke gebieden zijn populair om te winkelen in Rotterdam?" Dus ging ik informatie opzoeken op internet. Vervolgens maakte ik een collage van de informatie die ik had gevonden. Wanneer mijn collage af was, ging ik verder met mijn moodboard. Ik richtte met mijn moodboard meer op evenementen van studenten van de opleiding Leisure en event management.</p>

![Onderzoek Collage](../images/2017-09-06-Onderzoek collage.jpeg)
 
<p> Na het maken van moodboard hiel ik een pauze. Daarna kregen we een uitleg over bloggen maken. 
Helaas liep bij het site Stackedit steeds vast dus het was lastig voor mij. Ik ging toen met MarkdownPad werken, maar dat werkte niet helemaal goed. Ik kon geen afbeeldingen plaatsten. Dus ging ik met Word weer verder. Na een uurtje ging ik aan de slag met mijn groep met de enquête. </p>

----

<h3> donderdag 7 september </h3>
<p> Het was vandaag best wel een slecht weer, af en toe beetje zon. Ik ben een beetje verkouden. Om 10 uur hadden we afgesproken om met zijn allen aan onze Design Challenge te werken. We hadden met elkaar over het resultaat van de enquête besproken. Wij analyseren het en verwerk het. </p>  

[SCREENSHOTS] 

<p> Om 1 uur begint onze officiële les en dat is workshop. Mijn groepje en ik moesten een (..) maken en vervolgens presenteren aan de klas. Het was best wel eng voor mij, want ik ben bang dat ik over mijn woorden struikel en ik ben altijd een slechte prater. Gelukkig ging het goed. </p>
